import java.io.{BufferedWriter, File, FileWriter}
import scala.io.Source;

/**
  * Created by ramdas on 27/12/17.
  */
case class FileOperationException(private val message: String = "", private val cause: Throwable = None.orNull) extends Exception(message, cause)

object ReadFilesFromDirectory {

  def getFilesFromFolderByExtension(folderPath:String,extension: String): List[File] ={
      try{
        new File(folderPath).listFiles.filter(_.getName.endsWith(extension)).toList
      }catch {
        case e:Exception => println("Exception Cause: " + e.getCause() + "Message" + e.getMessage );List.empty
      }
  }

  def capitalizeFirstLetterOfLine(line:String): String = {
    line.split(' ').map(_.capitalize).mkString(" ")+" \n"
  }

  def transformAndWriteFilesToDestinationFolder(files:List[File],destinationFolder:String,stringTransformLogic :(String => String )): Unit = {
    for(file <- files){
      try {
        val fileToConvert = new File(destinationFolder + file.getName)
        val bw = new BufferedWriter(new FileWriter(fileToConvert))
        Source.fromFile(file).getLines().foreach(line => bw.write(stringTransformLogic(line)))
        bw.close()
      }catch {
        case e:Exception => println("Exception Cause: " + e.getCause + "Message" + e.getMessage );List.empty
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val source = "/home/ramdas/Pleasedeletethis-folder-nousefurther"
    val destination = "/home/ramdas/Pleasedeletethis-folder-nousefurther/output/"
    transformAndWriteFilesToDestinationFolder(getFilesFromFolderByExtension(source,".text"),destination,capitalizeFirstLetterOfLine);
  }

}